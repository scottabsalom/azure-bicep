targetScope = 'subscription'

param rgName string = 'rgdatalake'
param location string = 'uksouth'

param dlName string

resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name:rgName
  location:location
}

module dataLake './datalake.bicep' = {
  name:'dataLake'
  scope:rg
  params:{
    dlName:dlName
  }
}

output datalakeId string = dataLake.outputs.datalakeId
