
param dlName string
param location string = resourceGroup().location

resource dl 'Microsoft.Storage/storageAccounts@2021-02-01' = {
  name:dlName
  location:location
  kind:'StorageV2'
  sku:{
    name:'Standard_LRS'
  }
  properties:{
    isHnsEnabled:true
  }
}

output datalakeId string = dl.id
