param saName string
param location string = resourceGroup().location

var storageSku = 'Standard_LRS'

resource stg 'Microsoft.Storage/storageAccounts@2021-02-01' = {
  name: saName
  location: location
  kind: 'StorageV2'
  sku: {
    name: storageSku
  }
}

output storageId string = stg.id
