targetScope = 'subscription'

param rgName string = 'rgstorage'
param location string = 'uksouth'

param saName string

resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name:rgName
  location:location
}

module modStorage './storage.bicep' = {
  name:'StorageModule'
  scope:rg
  params:{
    saName:saName
  }
}

output storageId string = modStorage.outputs.storageId
