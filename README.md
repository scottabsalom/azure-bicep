# Azure Bicep

Sandbox for investigating bicep files

# Initial Usage

New-AzDeployment -TemplateFile ./main.bicep -Location '<tbc>'

# Clean Up all resources in a resource group

New-AzResourceGroupDeployment -ResourceGroupName MyResourceGroup -Mode Complete -TemplateFile .\rm-all.bicep -Force -Verbose