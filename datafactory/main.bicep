targetScope = 'subscription'

param rgName string = 'rgdatafactory'
param location string = 'uksouth'

param dfName string 

resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name:rgName
  location:location
}

module datafactory './datafactory.bicep' = {
  name:'datafactory'
  scope:rg
  params:{
    dfName:dfName
  }
}

output datafactoryId string = datafactory.outputs.datafactoryId
