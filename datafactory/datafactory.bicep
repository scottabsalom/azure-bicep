param dfName string
param location string = resourceGroup().location

resource df 'Microsoft.DataFactory/factories@2018-06-01' = {
  name:dfName
  location:location
}

output datafactoryId string = df.id
