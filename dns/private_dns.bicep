param zoneName string

resource privateDNS 'Microsoft.Network/privateDnsZones@2020-06-01' = {
  location: 'global'
  name: zoneName
}

output Id string = privateDNS.id
