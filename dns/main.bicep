targetScope = 'subscription'

param zoneName string = 'scottabsalom.com'

param rgName string = 'rgdns'
param location string = 'UK South'

resource rg 'Microsoft.Resources/resourceGroups@2021-01-01' = {
  name: rgName
  location: location
}

module modPrivateDNS './private_dns.bicep' = {
  name: 'privateDNSDeploy'
  params: {
    zoneName: zoneName
  }
  scope: rg
}

module modPublicDNS './public_dns.bicep' = {
  name: 'publicDNSDeploy'
  params: {
    zoneName: zoneName
  }
  scope: rg
}

output privateDNSId string = modPrivateDNS.outputs.Id
output publicDNSId string = modPublicDNS.outputs.Id
