param zoneName string

resource publicDNS 'Microsoft.Network/dnsZones@2018-05-01' = {
  location: 'global'
  name: zoneName
}

output Id string = publicDNS.id

